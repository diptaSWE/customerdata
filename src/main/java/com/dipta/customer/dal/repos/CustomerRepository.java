package com.dipta.customer.dal.repos;

import org.springframework.data.repository.CrudRepository;

import com.dipta.customer.dal.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

}
