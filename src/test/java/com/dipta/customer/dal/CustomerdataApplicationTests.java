package com.dipta.customer.dal;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.dipta.customer.dal.entities.Customer;
import com.dipta.customer.dal.repos.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerdataApplicationTests {

	@Autowired
	private CustomerRepository customerRepository;
	@Test
	public void contextLoads() {
	}
	@Test
	public void testCreateCustomer(){
		Customer customer = new Customer();
		customer.setCname("Dipta");
		customer.setCemail("rabbani@gmail.com");
		customerRepository.save(customer);
	}
	
	@Test
	public void testReadCustomer(){
		Customer customer = customerRepository.findById(2).get();
		System.out.println(customer);
	}
	
	@Test
	public void testUpdateCustomer(){
		Customer customer = customerRepository.findById(2).get();
		customer.setCemail("gr.rishad@gmail.com");
		customerRepository.save(customer);
	}
	@Test
	public void testDeleteCustomer(){
		customerRepository.deleteById(3);
	}

}
